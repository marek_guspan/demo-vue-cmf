import List from '@/views/List.vue'
import Form from '@/views/Form.vue'

const routes = [
    { path: '/', redirect: '/appcustomer/pet/pets' },
    { path: '/:vendor/:plugin/:controller', component: List },
    { path: '/:vendor/:plugin/:controller/update/:id', component: Form },
    { path: '/:vendor/:plugin/:controller/create', component: Form }
]

export default routes