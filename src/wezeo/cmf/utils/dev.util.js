import axios from 'axios'

export function getHost() {
	return localStorage['ocmsHost'] || window['ocmsHost'] || 'http://localhost/doginni-ocms'
}

export async function dev_onLogin() {
	axios.get(`${getHost()}/backend/backend/auth/signin`, { withCredentials: true }).then((res) => {
		const csrfToken = res.data.split('name="csrf-token" content="')[1].split('"')[0]

		// ulozenie csrf tokenu pre post requesty
		localStorage.csrfToken = csrfToken;

		const body = new FormData()
		body.set('login', 'admin')
		body.set('password', 'admin')
		body.set('postback', "1")
		body.set('_token', csrfToken)

		axios.post(`${getHost()}/backend/backend/auth/signin`, body, { withCredentials: true })
			.then(() => {
				alert('logged in')
			})
	})
}
