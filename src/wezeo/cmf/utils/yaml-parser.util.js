const yaml = require('js-yaml')

export function parseFields(data)
{
    let fields = []
    let parsedData = yaml.safeLoad(data)

    Object.entries(parsedData.fields).forEach(prop => {
      let obj = {
        label: prop[1].label,
        model: prop[0],
        styleClasses: 'col-6'
      }
      switch (prop[1].type) {
        case 'switch':
          obj['type'] = prop[1].type
          obj['textOn'] = prop[1].on
          obj['textOff'] = prop[1].off
          obj['type'] = prop[1].type
          break;

        case 'recordfinder':
          obj['type'] = 'input'
          obj['inputType'] = 'text'
          obj['model'] = prop[0] + '_id'
          break;

        case 'text':
          obj['type'] = 'input'
          obj['inputType'] = 'text'
          break;

        case 'dropdown':
          obj['type'] = 'select'
          obj['values'] = []
          Object.entries(prop[1].options).forEach(option => {
            obj['values'].push({
              id: option[0],
              name: option[1]
            })
          })
        }
      fields.push(obj)
    })

    return fields
}

export function parseColumns(data)
{
    let columns = []
    let parsedData = yaml.safeLoad(data)
    Object.entries(parsedData.columns).forEach(prop => {
        const obj = {}
        obj.field = prop[0],
        obj.label = prop[1].label.toUpperCase()
        if (prop[0].includes('id')) {
            obj.type = 'number'
        }
        console.log(obj)
        columns.push(obj)
    })

    return columns
}