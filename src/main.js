import Vue from 'vue'
import App from './App.vue'
import routes from '@/router/routes.js'
import VueRouter from 'vue-router'

import VueFormGenerator from 'vue-form-generator'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import '@/assets/vfg.css'
import '@/assets/global.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

//spravit to akoze plugin ako v doginni, october struktura
import '@/globalComponents'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueRouter)
Vue.use(VueFormGenerator)

Vue.config.productionTip = false

const router = new VueRouter({routes: routes, mode: "history"});

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
