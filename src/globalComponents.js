import Vue from 'vue'

Vue.component('CmfHeader', () => import('@/wezeo/cmf/cmf-Header.vue'))
Vue.component('CmfList', () => import('@/wezeo/cmf/cmf-List.vue'))
Vue.component('CmfForm', () => import('@/wezeo/cmf/cmf-Form.vue'))
Vue.component('CmfLoginBtn', () => import('@/wezeo/cmf/cmf-LoginBtn.vue'))
Vue.component('CmfAddBtn', () => import('@/wezeo/cmf/cmf-AddBtn.vue'))
Vue.component('CmfDeleteBtn', () => import('@/wezeo/cmf/cmf-DeleteBtn.vue'))